#! /usr/bin/env bash

workspace=$(cd $(dirname $0); pwd)

python_exe=/home/vcap/app/.python/bin/python
cd /home/vcap/app/.setuptools && $python_exe setup.py install &> /dev/null
cd /home/vcap/app/.pip && $python_exe setup.py install &> /dev/null
cd /home/vcap/app/.gunicorn && $python_exe setup.py install &> /dev/null

cd $workspace
if [ -f requirements.txt ]; then
    .python/bin/pip install -r requirements.txt --exists-action=w
fi
/home/vcap/app/.python/bin/gunicorn wsgi:application -c /home/vcap/app/config/gunicorn.conf