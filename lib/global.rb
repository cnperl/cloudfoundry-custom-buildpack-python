#! /usr/bin/ruby -w
# coding: utf-8
# author: Ulric Qin
# mail: qinxiaohui@xiaomi.com
require 'yaml'

class Global

  attr_reader :build_path, :cache_path

  def initialize(build_path, cache_path)
    @build_path = build_path
    @cache_path = cache_path
  end

  def build_pack_dir
    @build_pack_dir ||= File.dirname(File.dirname(__FILE__))
  end

  def url_yml_path
    @url_yml_path ||= File.join(build_pack_dir, 'config', 'url.yml')
  end

  def dependency_yml_path
    @dependency_yml_path ||= File.join(build_path, 'config', 'dependency.yml')
  end

  def dependency_yml
    if File.exist? dependency_yml_path
      @dependency_yml ||= YAML.load_file(dependency_yml_path)
    else
      return nil
    end
  end

  def url_yml
    @url_yml ||= YAML.load_file(url_yml_path)
  end

  def download_url(down_target)
    base = url_yml['base']
    if dependency_yml and not dependency_yml[down_target].nil?
      File.join(base, dependency_yml[down_target])
    else
      File.join(base, url_yml['default'][down_target])
    end
  end

  def home_target_dir(target_mod)
    File.join(build_path, '.'+target_mod)
  end

end