#! /usr/bin/ruby -w
# coding: utf-8
# author: Ulric Qin
# mail: qinxiaohui@xiaomi.com
require 'fetcher'
require 'fileutils'
require 'global'
require 'system_util'

class MainPack

  attr_reader :global

  def initialize(global)
    @global = global
  end

  def compile
    Fetcher.down_to_home(global, 'python', 'bin/python')
    Fetcher.down_to_home(global, 'setuptools', 'setup.py')
    Fetcher.down_to_home(global, 'pip', 'setup.py')
    Fetcher.down_to_home(global, 'gunicorn', 'setup.py')

    user_gunicorn_conf = File.join(global.build_path, 'config', 'gunicorn.conf')
    dir = File.dirname(user_gunicorn_conf)
    FileUtils.mkdir_p(dir)

    if not File.exist?(user_gunicorn_conf)
      FileUtils.cp(File.join(global.build_pack_dir, 'config', 'gunicorn.conf'), user_gunicorn_conf)
    end

    FileUtils.cp(File.join(global.build_pack_dir, 'bin', 'boot.sh'), File.join(global.build_path, 'boot.sh'))

  end

end